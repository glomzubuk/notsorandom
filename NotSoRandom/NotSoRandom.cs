﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using LLHandlers;
using HarmonyLib;
using BepInEx;
using BepInEx.Configuration;
using LLBML;
using LLBML.Players;

namespace NotSoRandom
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class NotSoRandom : BaseUnityPlugin
    {
        private static Dictionary<Stage, ConfigEntry<bool>> stageConfigs = new Dictionary<Stage, ConfigEntry<bool>>();
        private static Dictionary<Character, ConfigEntry<bool>> characterConfigs = new Dictionary<Character, ConfigEntry<bool>>();
        private IEnumerable<Character> characterList = null;

        // Awake is called once when both the game libs and the plug-in are loaded
        void Awake()
        {
            Logger.LogInfo("Hello, world!");
            Harmony harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            harmoInstance.PatchAll(typeof(RandomCharactersPatch));
            harmoInstance.PatchAll(typeof(RandomStagesPatch));

        }

        // Start is called once when every inital game components (and plugins) are loaded
        void Start()
        {
            this.InitCharactersConfig();
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        private void Update()
        {
            if (stageConfigs.Count == 0 && StageHandler.stagesAll != null)
            {
                InitStagesConfig();
            }
        }

        private readonly bool enableCharDebug = false;
        private readonly bool enableStageDebug = false;
        private void OnGUI()
        {
            if (enableCharDebug || enableStageDebug)
            {
                GUIStyle bold = new GUIStyle();
                bold.fontStyle = FontStyle.Bold;
                bold.normal.textColor = Color.red;
                if (enableCharDebug)
                {
                    Player player0 = Player.GetPlayer(0);
                    GUI.Label(new Rect(10, 10, 200, 25), "Player number: " + player0.nr.ToString(), bold);
                    GUI.Label(new Rect(10, 35, 200, 25), "Character: " + player0.Character.ToString(), bold);
                    GUI.Label(new Rect(10, 60, 200, 25), "CharacterSelected: " + player0.CharacterSelected.ToString(), bold);
                    GUI.Label(new Rect(10, 85, 500, 25), "CharacterSelectedRandom: " + player0.CharacterSelectedIsRandom.ToString(), bold);

                    Player player1 = Player.GetPlayer(1);
                    GUI.Label(new Rect(310, 10, 200, 25), "Player number: " + player1.nr.ToString(), bold);
                    GUI.Label(new Rect(310, 35, 200, 25), "Character: " + player1.Character.ToString(), bold);
                    GUI.Label(new Rect(310, 60, 200, 25), "CharacterSelected: " + player1.CharacterSelected.ToString(), bold);
                    GUI.Label(new Rect(310, 85, 500, 25), "CharacterSelectedRandom: " + player1.CharacterSelectedIsRandom.ToString(), bold);
                }
                if (enableStageDebug)
                {
                    GUI.Label(new Rect(10, 10, 200, 25), "CurrentStage: " + StageHandler.curStage.ToString(), bold);
                }
            }
        }

        public static Character[] GetSkipCharacters()
        {
            List<Character> charList = new List<Character>();
            foreach (var pair in characterConfigs.Where((charConfig) => charConfig.Value.Value == false))
            {
                charList.Add(pair.Key);
            }
            return charList.ToArray();
        }

        public static List<Stage> GetValidStages()
        {
            List<Stage> stageList = new List<Stage>();
            foreach (var pair in stageConfigs.Where((stageConfig) => stageConfig.Value.Value == true))
            {
                stageList.Add(pair.Key);
            }
            return stageList;
        }

        private void InitCharactersConfig()
        {
            Logger.LogInfo("Initializing characters configurations");
            //TODO Move this where it can support additionnal characters
            if (characterList == null)
                characterList = CharacterApi.GetPlayableCharacters();

            Config.Bind(
                "Characters",
                "mm_header_Characters",
                "Characters:",
                "modmenu_header"
            );
            foreach (Character character in characterList)
            {
                characterConfigs.Add(character, Config.Bind(
                    "Characters",
                    "Enable" + character.ToString(),
                    true,
                    "Should " + character.ToString() + " be able to be randomed"
                ));
            }
        }
        private void InitStagesConfig()
        {
            Logger.LogInfo("Initializing Stage configurations");
            Config.Bind(
                "Stages",
                "mm_header_stages",
                "Stages:",
                "modmenu_header"
            );
            foreach (Stage stage in StageHandler.stagesAll)
            {

                stageConfigs.Add(stage, Config.Bind(
                    "Stages",
                    "Enable" + stage.ToString(),
                    true,
                    "Should " + stage.ToString() + " be able to be randomed"
                ));
            }
        }
    }
}
