﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using LLHandlers;
using HarmonyLib;

namespace NotSoRandom
{
    
    public static class RandomCharactersPatch
    {
        // There we specify what to patch, in this case it's DetermineCharacter in the Player class, there's way to choose the methods in case of ambiguity, i' ll provide another example
        [HarmonyPatch(typeof(ALDOKEMAOMB), "CHDHDGAHNPB")]
        // Prefix here is the same as it was before, it runs before the target method, and there can be multiple per method
        [HarmonyPrefix]
        // Method name doesn't matter if you use annotations, i just like clear naming
        // Arguments have naming conventions to get what you need in the patched environement, __instance returns the instance for example
        // All theses can be found here: https://github.com/BepInEx/HarmonyX/wiki/Patch-parameters
        // Prefixes must return a boolean that determine if the target method should run after execution, true is "yes it should" (the opposite of what we had with Cecil.Inject)
        // Altering the return value can be done by putting a "ref Type __result" argument
        // Passed arguments can be either positional or explicit with __0, __1, etc (i don't have a preference, but sometimes it's clearer to have the same name for an arg) 
        public static bool PlayerDetermineCharacter_Prefix(ref ALDOKEMAOMB __instance, Character[] __0)
        {
            if (__0 != null)
                return true;

            if (__instance.GAFCIHKIGNM && __instance.IMJLOPPPIJM && __instance.LALEEFJMMLH != Character.NONE)
            {
                Character[] nsrSkips = NotSoRandom.GetSkipCharacters();
                if (nsrSkips.Length > 0)
                {
                    __instance.LALEEFJMMLH = __instance.HGPNPNPJBMK(nsrSkips);
                    __instance.AIINAIDBHJI = __instance.GLCHOPBLHEB(false);
                    return false;
                }
            }
            return true;
        }
    }

    public static class RandomStagesPatch
    {
        [HarmonyPatch(typeof(LLHandlers.StageHandler), "GetRandomStage")]
        [HarmonyPrefix]
        public static bool GetRandomStage_Prefix(ref Stage __result, bool only_unlocked, StageRandom which)
        {
            List<Stage> list = (which != StageRandom.ANY_2D) ? StageHandler.stages3d : StageHandler.stages2d;
            if (only_unlocked)
            {
                if (JOMBNFKIHIC.GDNFJCCCKDM)
                {
                    list = EPCDKLCABNC.JKGLOFIECCP(list);
                }
                else
                {
                    list = EPCDKLCABNC.JKGLOFIECCP(list);
                }
            }
            if (list.Count == 0 && which == StageRandom.ANY_2D)
            {
                __result = StageHandler.GetRandomStage(only_unlocked, StageRandom.ANY_3D);
                return false;
            }

            List<Stage> nsrStages = NotSoRandom.GetValidStages();
            if (nsrStages.Count > 0)
            {
                list = list.Intersect(nsrStages).ToList();
            }
            __result = list[UnityEngine.Random.Range(0, list.Count)];
            return false;
        }
    }

    public static class OldPatches
    {
        // Specifying an argument type to target a specific method from multiple definitions with the same name
        [HarmonyPatch(typeof(HDLIJDBFGKN), "OAACLLGMFLH", typeof(int[]))]
        [HarmonyPrefix]
        public static bool GameStartWithArgs_Prefix(HDLIJDBFGKN __instance, int[] __0)
        {
            Debug.Log("Debug: The OAACLLGMFLH(int[]) isn' t a fake");

            Debug.Log("Debug: " + new System.Diagnostics.StackTrace());
            return true;
        }

        [HarmonyPatch(typeof(HDLIJDBFGKN), "OAACLLGMFLH")]
        [HarmonyPatch(new System.Type[0])]
        [HarmonyPrefix]
        public static bool GameStart_Prefix(HDLIJDBFGKN __instance)
        {
            Debug.Log("Debug: " + new System.Diagnostics.StackTrace());
            Debug.Log("Debug: The OAACLLGMFLH isn' t a fake");

            return true;
        }

        [HarmonyPatch(typeof(ALDOKEMAOMB), "IJOLFCPOMJM", typeof(Character))]
        public static bool SetPlayerCharacter_Prefix()
        {
            Debug.Log("Debug: " + new System.Diagnostics.StackTrace());
            return false;
        }

        [HarmonyPatch(typeof(ALDOKEMAOMB), "LALEEFJMMLH", typeof(Character))]
        public static bool SetPlayerCharacterSetter_Prefix()
        {
            Debug.Log("Debug: " + new System.Diagnostics.StackTrace());
            return false;
        }

        //This is probably a fake method
        [HarmonyPatch(typeof(ALDOKEMAOMB), "PMLHKDFAKGF")]
        [HarmonyPrefix]
        public static bool PMLHKDFAKGF_Prefix(ALDOKEMAOMB __instance, Character __0)
        {
            Debug.Log("Debug:Change character was called | Player number: " + __instance.CJFLMDNNMIE + " | Character: " + __0.ToString());

            __instance.LALEEFJMMLH = __0;
            return false;
        }
    }
}
